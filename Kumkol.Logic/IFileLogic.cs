using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Kumkol.Logic
{
    public interface IFileLogic
    {
        Task<int> UploadFile(IFormFile file);
        Task<Tuple<MemoryStream, string>> DownloadFile(int id);
    }
}