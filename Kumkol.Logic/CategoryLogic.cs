using System.Collections.Generic;
using System.Threading.Tasks;
using Kumkol.Data.Models;
using Kumkol.Data.Repos;

namespace Kumkol.Logic
{
    public class CategoryLogic : ICategoryLogic
    {
        private readonly ICategoryRepo _repo;
        public CategoryLogic(ICategoryRepo repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<CategoryModel>> GetLists()
        {
            var sql = "select * from public.\"Categories\"";
            return await _repo.Find(sql);
        }

        public async Task AddToLists(CategoryModel model)
        {
            var sql = "insert into public.\"Categories\"(";
            sql += "\"Name\", \"Data\")";
            sql += "values (@Name, cast(@Data as Json));";
            await _repo.Execute(sql, model);
        }
        
        public async Task Delete(int id)
        {
            var sql = "delete from public.\"Categories\" where \"Id\" = @Id;";
            await _repo.Execute(sql, new CategoryModel{Id = id});
        }
    }
}