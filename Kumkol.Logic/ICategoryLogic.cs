using System.Collections.Generic;
using System.Threading.Tasks;
using Kumkol.Data.Models;

namespace Kumkol.Logic
{
    public interface ICategoryLogic
    {
        Task<IEnumerable<CategoryModel>> GetLists();
        Task AddToLists(CategoryModel model);
        Task Delete(int id);
    }
}