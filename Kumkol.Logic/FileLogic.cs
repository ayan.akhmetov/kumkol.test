using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using Dapper;
using Kumkol.Data.Models;
using Kumkol.Data.Repos;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;

namespace Kumkol.Logic
{
    public class FileLogic : IFileLogic
    {
        private readonly IFileRepo _repo;
        private readonly IHostingEnvironment _env;
        public FileLogic(IFileRepo repo, IHostingEnvironment env)
        {
            _env = env;
            _repo = repo;
        }

        public async Task<int> UploadFile(IFormFile file)
        {
            if (file == null) throw new ArgumentException("file is null");
            var path = Path.Combine(  
                Directory.GetCurrentDirectory(), "files",   
                file.FileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            var sql = "insert into public.\"Files\"(";
            sql += "\"Path\", \"FileName\", \"ContentType\")";
            sql += "values (@Path, @FileName, @ContentType) RETURNING \"Id\";";
            return await _repo.ExecuteReturnId(sql, new FileModel{ FileName = file.FileName, Path = path, ContentType = file.ContentType});
        }

        public async Task<Tuple<MemoryStream, string>> DownloadFile(int id)
        {
            var sql = $"select \"FileName\", \"ContentType\" from public.\"Files\" where \"Id\" = {id} limit 1";
            var result = await _repo.Find(sql);
            var path = Path.Combine(Directory.GetCurrentDirectory(), "files", result.First().FileName);  
  
            var memory = new MemoryStream();  
            using (var stream = new FileStream(path, FileMode.Open))  
            {  
                await stream.CopyToAsync(memory);  
            }  
            memory.Position = 0;
            return new Tuple<MemoryStream, string>(memory, result.First().ContentType);
        }
    }
}