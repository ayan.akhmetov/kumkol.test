using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Kumkol.Data.Models;
using Kumkol.Data.Repos;

namespace Kumkol.Logic
{
    public class ProductLogic : IProductLogic
    {
        private readonly IProductRepo _repo;
        public ProductLogic(IProductRepo repo)
        {
            _repo = repo;
        }
        
        public async Task<IEnumerable<ProductModel>> GetLists(ProductModel model)
        {
            var sql = "select * from public.\"Products\"";
            var addAnd = false;
            if (ValidModel(model))
                sql += " where ";
            foreach (var item in model.GetType().GetProperties())
            {
                switch (item.Name)
                {
                    case "Id":
                        if (model.Id != null)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"Id\" = {model.Id} ";
                            addAnd = true;
                        }
                        break;
                    case "Name":
                        if (model.Name != null)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"Name\" like '%{model.Name}%' ";
                            addAnd = true;
                        }
                        break;
                    case "Description":
                        if (model.Description != null)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"Description\" like '{model.Description}' ";
                            addAnd = true;
                        }
                        break;
                    case "Price":
                        if (model.Price != 0)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"Price\" = '{model.Price}' ";
                            addAnd = true;
                        }
                        break;
                    case "Data":
                        if (model.Data != null)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"Data\" @> '{model.Data}' ";
                            addAnd = true;
                        }
                        break;
                    case "CategoryId":
                        if (model.CategoryId != null)
                        {
                            sql += addAnd ? " and " : "";
                            sql += $" \"CategoryId\" = {model.CategoryId} ";
                            addAnd = true;
                        }
                        break;
                }
            }
            return await _repo.Find(sql);
        }

        public async Task AddToLists(ProductModel model)
        {
            var sql = "insert into public.\"Products\"(";
            sql += "\"Name\", \"Description\", \"Price\", \"FileId\", \"CategoryId\", \"Data\")";
            sql += "values (@Name, @Description, @Price, @FileId, @CategoryId, cast(@Data as Json));";
            await _repo.Execute(sql,model);
        }
        
        public async Task Delete(int id)
        {
            var sql = "delete from public.\"Products\" where \"Id\" = @Id;";
            await _repo.Execute(sql, new ProductModel{Id = id});
        }

        private static bool ValidModel(ProductModel model)
        {
            return model.Id != null || model.Data != null || model.Description != null || model.Name != null 
                   || model.FileId != null || model.Price != 0 || model.CategoryId != null;
        }
    }
}