using System.Collections.Generic;
using System.Threading.Tasks;
using Kumkol.Data.Models;

namespace Kumkol.Logic
{
    public interface IProductLogic
    {
        Task<IEnumerable<ProductModel>> GetLists(ProductModel model);
        Task AddToLists(ProductModel model);
        Task Delete(int id);
    }
}