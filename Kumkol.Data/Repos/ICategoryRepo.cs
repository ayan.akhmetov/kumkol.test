using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public interface ICategoryRepo : IBaseRepo<CategoryModel>
    {
        
    }
}