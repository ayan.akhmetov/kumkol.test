using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public class ProductRepo : BaseRepo<ProductModel>, IProductRepo
    {
        public ProductRepo(IDb db) : base(db)
        {
        }
    }
}