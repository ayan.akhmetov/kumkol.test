using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kumkol.Data.Repos
{
    public class BaseRepo<TEntity> : IBaseRepo<TEntity>
    {
        private  IDb _db { get; }
        public BaseRepo(IDb db)
        {
            _db = db ?? throw new ArgumentException(nameof(db));
        }

        public async Task Execute(string sql, TEntity entity)
        {
            await _db.ExecuteAsync(sql, entity);
        }
        
        public async Task<int> ExecuteReturnId(string sql, TEntity entity)
        {
            return await _db.ExecuteReturnIdAsync(sql, entity);
        }

        public async Task<IEnumerable<TEntity>> Find(string sql, object parameters = null)
        {
            return await _db.SelectAsync<TEntity>(sql, parameters);
        }
    }
}