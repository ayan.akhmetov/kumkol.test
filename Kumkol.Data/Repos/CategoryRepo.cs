using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public class CategoryRepo : BaseRepo<CategoryModel>, ICategoryRepo
    {
        public CategoryRepo(IDb db) : base(db)
        {
        }
    }
}