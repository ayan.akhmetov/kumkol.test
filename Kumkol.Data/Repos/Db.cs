using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Npgsql;

namespace Kumkol.Data.Repos
{
    public class Db : IDb
    {
        private readonly Func<NpgsqlConnection> _dbConnectionFactory;
        private readonly int _timeOut;

        public Db(Func<NpgsqlConnection> dbConnectionFactory, int timeOut)
        {
            _dbConnectionFactory =
                dbConnectionFactory ?? throw new ArgumentNullException(nameof(dbConnectionFactory));
            _timeOut = timeOut;
        }

        private async Task<T> CommandAsync<T>(Func<IDbConnection, IDbTransaction, int, Task<T>> command)
        {
            using (IDbConnection connection = _dbConnectionFactory())
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        var result = await command(connection, transaction, _timeOut);
                        transaction.Commit();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        public async Task ExecuteAsync(string sql, object parameters)
        {
            await CommandAsync(async (conn, trn, timeout) =>
            {
                await conn.ExecuteAsync(sql, parameters, trn, timeout);
                return 1;
            });
        }
        
        public async Task<int> ExecuteReturnIdAsync(string sql, object parameters)
        {
            return await CommandAsync(async (conn, trn, timeout) =>
            {
                var result = await conn.QuerySingleAsync<int>(sql, parameters, trn, timeout);
                return result;
            });
        }

        public async Task<List<T>> SelectAsync<T>(string sql, object parameters)
        {
            return await CommandAsync<List<T>>(async (conn, trn, timeout) =>
            {
                var result = (await conn.QueryAsync<T>(sql, parameters, trn, timeout)).ToList();
                return result;
            });
        }
    }
}

