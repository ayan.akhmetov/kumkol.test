using System.IO;
using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public class FileRepo : BaseRepo<FileModel>, IFileRepo
    {
        public FileRepo(IDb db) : base(db)
        {
        }
    }
}