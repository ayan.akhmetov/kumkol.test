using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public interface IProductRepo : IBaseRepo<ProductModel>
    {
        
    }
}