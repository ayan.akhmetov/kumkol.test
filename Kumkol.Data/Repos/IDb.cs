using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kumkol.Data.Repos
{
    public interface IDb
    {
        Task ExecuteAsync(string sql, object parameters);
        Task<int> ExecuteReturnIdAsync(string sql, object parameters);
        Task<List<T>> SelectAsync<T>(string sql, object parameters);
    }
}