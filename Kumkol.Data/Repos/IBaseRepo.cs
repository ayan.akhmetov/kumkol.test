using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kumkol.Data.Repos
{
    public interface IBaseRepo<TEntity>
    {
        Task Execute(string sql, TEntity entity);
        Task<int> ExecuteReturnId(string sql, TEntity entity);
        Task<IEnumerable<TEntity>> Find(string sql, object parameters = null);
    }
}