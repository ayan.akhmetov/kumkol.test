using Kumkol.Data.Models;

namespace Kumkol.Data.Repos
{
    public interface IFileRepo : IBaseRepo<FileModel>
    {
        
    }
}