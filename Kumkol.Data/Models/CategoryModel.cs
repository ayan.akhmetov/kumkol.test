using System.ComponentModel.DataAnnotations;

namespace Kumkol.Data.Models
{
    public class CategoryModel
    {
        public int? Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Data { get; set; }
    }
}