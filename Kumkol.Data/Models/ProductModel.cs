using Microsoft.AspNetCore.Http;

namespace Kumkol.Data.Models
{
    public class ProductModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int? FileId { get; set; }
        public string Data { get; set; }
        public int? CategoryId { get; set; }
    }
}