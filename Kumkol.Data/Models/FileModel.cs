namespace Kumkol.Data.Models
{
    public class FileModel
    {
        public int? Id { get; set; }
        public string Path { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}