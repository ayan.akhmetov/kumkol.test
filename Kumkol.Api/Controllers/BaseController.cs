using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace Kumkol.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : Controller
    {
        protected IActionResult ExceptionResult(Exception ex)
        {
            var controllerName = ControllerContext.ActionDescriptor.ControllerName;
            var actionName = ControllerContext.ActionDescriptor.ActionName;
            var msg = $"{controllerName} {actionName} {ex.Message}";

            if (ex is ArgumentException)
            {
#if DEBUG
                return BadRequest(ex.Message);
#else
            return BadRequest(msg);
#endif
            }

            Response.StatusCode = (int) HttpStatusCode.InternalServerError;
#if DEBUG
            return Json(ex);
#else
            return Json(msg);
#endif
        }
    }
}