using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using Kumkol.Logic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kumkol.Api.Controllers
{
    public class FileController : BaseController
    {
        private readonly IFileLogic _logic;
        public FileController(IFileLogic logic)
        {
            _logic = logic;
        }
        
        [HttpPost("Upload")]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            try
            {
                var result = await _logic.UploadFile(file);
                return Ok(result);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
        
        [HttpPost("Download")]
        public async Task<IActionResult> Download([FromQuery][Required]int id)  
        {  
            try
            {
                var result = await _logic.DownloadFile(id);
                return File(result.Item1, result.Item2);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        } 
    }
}