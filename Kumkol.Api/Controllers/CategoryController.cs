using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Kumkol.Data.Models;
using Kumkol.Logic;
using Microsoft.AspNetCore.Mvc;

namespace Kumkol.Api.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ICategoryLogic _logic;
        public CategoryController(ICategoryLogic logic)
        {
            _logic = logic;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                var result = await _logic.GetLists();
                return Ok(result);
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CategoryModel model)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                await _logic.AddToLists(model);
                return NoContent();
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
        
        [HttpDelete]
        public async Task<IActionResult> Delete([FromQuery][Required]int id)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                await _logic.Delete(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return ExceptionResult(e);
            }
        }
    }
}