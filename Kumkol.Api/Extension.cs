using Kumkol.Data.Repos;
using Kumkol.Logic;
using Microsoft.Extensions.DependencyInjection;

namespace Kumkol.Api
{
    public static class Extension
    {
        public static void Load(this IServiceCollection services)
        {
            services.AddTransient<ICategoryLogic, CategoryLogic>();
            services.AddTransient<IProductLogic, ProductLogic>();
            services.AddTransient<ICategoryRepo, CategoryRepo>();
            services.AddTransient<IProductRepo, ProductRepo>();
            services.AddTransient<IFileLogic, FileLogic>();
            services.AddTransient<IFileRepo, FileRepo>();
        }
    }
}