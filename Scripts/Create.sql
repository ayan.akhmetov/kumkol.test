CREATE TABLE public."Categories" (
    "Id" serial PRIMARY KEY,
    "Name" text,
    "Data" jsonb
);

CREATE TABLE public."Files"
(
    "Id" serial PRIMARY KEY,
    "Path" text,
    "FileName" text,
    "ContentType" text
);

CREATE TABLE public."Products" (
    "Id" serial PRIMARY KEY,
    "Name" text,
    "Description" text,
    "Price" real,
    "Data" jsonb,
    "FileId" integer REFERENCES public."Files" ("Id") ON DELETE CASCADE,
    "CategoryId" integer REFERENCES public."Categories" ("Id") ON DELETE CASCADE
);